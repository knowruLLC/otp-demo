﻿# Welcome to use OTP-demo
Here is the login page screenshot
![avatar](static/img/login.png)

# Install
### Install redis
```
$ sudo apt-get install redis-server
$ sudo service redis-server start
```
### Install pyenv, python 3.5, pipenv and requirements
```
$ curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
$ pyenv install 3.5.5
$ pyenv local 3.5.5
$ python -V
Python 3.5.5
$ pip -V
pip 9.0.1 from /home/ubuntu/.pyenv/versions/3.5.5/lib/python3.5/site-packages (python 3.5)
$ pip install pipenv
cd ~/otp_demo
pipenv shell
pip install -r requirements.txt
pip install gunicorn
```
### Modify envvars.py
```
os.environ['KNOWRU_AWS_ACCESS_KEY_ID'] = '' #enter the aws access key ID
os.environ['KNOWRU_AWS_SECRET_ACCESS_KEY'] = '' #enter the aws secret access key
os.environ['KNOWRU_AWS_REGION_NAME'] = '' #enter the aws region(e.g. 'us-west-1')
```
# Release
### Install and config supervisor
```
sudo apt-get install supervisor
sudo vim /etc/supervisor/conf.d/otp.conf
```
here is the content of otp.conf file.
```
[program:otp_demo]
command=/home/ubuntu/.local/share/virtualenvs/otp_demo-CwHvZBG9/bin/gunicorn otp_demo.wsgi:application -b 0.0.0.0:8000
directory=/home/ubuntu/otp_demo/
autostart=true
autorestart=true
startretries=10
exitcodes=0
stopsignal=KILL
stopwaitsecs=10
redirect_stderr=true
stdout_logfile=/home/ubuntu/logs/otp_demo/otp_demo_stdout.log
stderr_logfile=/home/ubuntu/logs/otp_demo/otp_demo_stderr.log
stopasgroup=true
```
restart supervisor
```
sudo service supervisor restart
```
### Install and config nginx
```
sudo apt-get -y install nginx
sudo vim /etc/nginx/sites-enabled/otp_demo
```
here is the content of otp_demo file
```
server {
    listen 80;
    server_name otp.knowru.com otp-demo.knowru.com;

    proxy_set_header X-Forwarded-Proto $scheme;
    if ( $http_x_forwarded_proto != 'https' ) {
        return 301 https://$host$request_uri;
    }

    access_log /home/ubuntu/logs/otp-access.log;
    error_log /home/ubuntu/logs/otp-access.log;

    root /home/ubuntu/otp_demo/;

    location / {
        proxy_pass http://127.0.0.1:8000;
        proxy_pass_header       Authorization;
        proxy_pass_header       WWW-Authenticate;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /static/ {
        root /home/ubuntu/otp_demo/;
    }

}
```
start the nginx service
```
sudo /usr/sbin/nginx -s reload
```