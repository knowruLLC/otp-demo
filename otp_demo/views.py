from django.http import JsonResponse
from django.views.generic import ListView
from django.shortcuts import render
from django.template import RequestContext
from django.core.cache import cache
from django.conf import settings
import random
import boto3


class Login(ListView):
	"""docstring for Login"""
	def __init__(self, *args, **kwargs):
		super(Login, self).__init__(*args, **kwargs)
		self.code = ''
		self.phone = ''
		self.context = {}
		self.context['status'] = 'fault'

	def get(self, request, *args, **kwargs):	
		self.object = None
		self.context['app_name'] = 'Knowru Bank'
		if "phone" in request.GET and "code" not in request.GET:
			self.phone = request.GET['phone']
			for num in range(0,6):
				self.code = self.code + str(random.randint(0, 9))
			client = boto3.client(
				"sns",
				aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
				aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
				region_name=settings.REGION_NAME
				)
			self.context['data'] = client.publish(
				PhoneNumber=self.phone,
				Message='【Knowru Bank】 Your one-time passcode is {code}. This passcode will expire in the next 24 hours. Please do not share this code with someone else.'.format(code=self.code)
				)
			self.context['msg'] = "We sent you an OTP to your phone. Please enter the verification code below."
			cache.set(self.phone, self.code, timeout=600)
			return JsonResponse(self.context)
		elif "code" in request.GET:
			if request.GET["code"]==cache.get(request.GET["phone"]):
				self.context['status'] = 'success'
			return JsonResponse(self.context)
		return render(request, 'login.html', self.context)

	def post(self, request, *args, **kwargs):
		if request.POST.get("phone")==self.phone and request.POST.get("code")==self.code:
			self.context['status'] = 'success'
		else:
			return JsonResponse(self.context)
		return render(request, 'login.html', self.context)
