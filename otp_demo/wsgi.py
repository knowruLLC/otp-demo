"""
WSGI config for otp_demo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

OTP_DIR = os.path.dirname(os.path.abspath(__file__))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'otp_demo.settings')
exec(compile(open(OTP_DIR + '/envvars.py').read(), OTP_DIR + '/envvars.py', 'exec'))

application = get_wsgi_application()
