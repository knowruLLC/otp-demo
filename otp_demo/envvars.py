import os

# AWS related
os.environ['KNOWRU_AWS_ACCESS_KEY_ID'] = ''
os.environ['KNOWRU_AWS_SECRET_ACCESS_KEY'] = ''
os.environ['KNOWRU_AWS_REGION_NAME'] = ''